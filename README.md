## Example Usage

```yaml
    - import_role:
        name: sled
      vars:
            hostname: stor
            bgp_ip: 10.99.0.10
            iface_name: eth1
            vtep_name: vtep2
            bridge_name: sledbr
            bridge_ip: 172.30.0.3
            as_num: 64710
            as_peer: 64701
            sled_img_path: /var/img
```